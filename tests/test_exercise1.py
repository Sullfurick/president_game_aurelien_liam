import unittest
from src import Cards, Deck


class TestCardsExercice1(unittest.TestCase):
    def test_card_constructor(self):
        self.assertTrue(isinstance(Cards.Card('A', '♡', 12), Cards.Card))

    def test_cards_equal_value(self):
        ace_of_hearts = Cards.Card('A', '♡', 12)
        ace_of_spades = Cards.Card('A', '♤', 12)
        self.assertEqual(ace_of_hearts, ace_of_spades, 'Two cards having '
                                                       'same name should be considered equal')

    def test_cards_comparison(self):
        ace_of_hearts = Cards.Card('A', '♡', 12)
        two_of_hearts = Cards.Card('2', '♡', 13)
        five_of_hearts = Cards.Card('5', '♡', 3)

        self.assertTrue(ace_of_hearts > five_of_hearts)
        self.assertTrue(two_of_hearts > ace_of_hearts > five_of_hearts,
                        'The two card is the highest card')
        self.assertTrue(five_of_hearts < two_of_hearts,
                        'The two card is the highest card')


class TestDeckExercice1(unittest.TestCase):
    def test_deck_has_52_cards(self):
        deck = Deck.Deck()
        self.assertEqual(len(deck.deck), 52, 'The president is a card game '
                                             'requiring 52 cards')

    def test_deck_shuffling(self):
        deck_1 = Deck.Deck()
        deck_2 = Deck.Deck()
        self.assertEqual(deck_1.deck, deck_2.deck, 'A new deck should not be automatically shuffled')
        deck_2.shuffle()
        self.assertNotEqual(deck_1.deck, deck_2.deck, 'Shuffling a deck '
                                                      'randomizes the '
                                                      'cards order')


if __name__ == '__main__':
    unittest.main()
