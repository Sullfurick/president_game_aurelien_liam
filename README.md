# **Jeu de cartes :** _Le Président_

## **Auteurs :** _Aurélien Gouriou & Liam Beauvais_

## **Installation**

```bash
git clone https://gitlab.com/Sullfurick/president_game_aurelien_liam.git
cd president_game_aurelien_liam
```
```bash
pip install --upgrade pip
pip install -r requirements.txt
py main.py
```

## _Règles du jeu :_

### **Ce jeu se joue de 3 à 6 joueurs.**

L'ensemble des cartes sont distribuées aux joueurs de la manière la plus homogène.
Ce jeu se joue par tours. Tant que quelqu'un peut et veut jouer, le tour continue et tourne dans le sens horaire.

Le premier joueur choisit des cartes d'une même valeur et les pose sur la tables.
Suite à celà, chaque joueur doit fournir autant de cartes que le joueur précédent, d'une valeur supérieure ou
égale.

Un joueur a le droit de sauter son tour et reprendre le tour d'après.
Un tour est fini lorsque plus personne ne joue. C'est alors le dernier à avoir joué qui ouvre la manche suivante. Ou si
un joueur pose un ou plusieurs deux. Ce qui mets immédiatement fin au tour.

L'objectif est d'être le premier à ne plus avoir de cartes. Ce joueur est alors déclaré président de la manche.
Les joueurs restants continuent à jouer jusqu'à ce qu'il n'y ait plus qu'un joueur qui ait des cartes en main, il est
alors déclaré 'trouduc'

On décide alors ou non de jouer une nouvelle manche. Ce sera le trouduc qui ouvrira la partie.

# **Résumé**

Ce projet effectué dans le cadre de notre Bachelor 3 DevOps (2022-2023), à l'EPSI Rennes, est destiné à nous 
faire travailler la POO (Programmation Orientée Objet) en Python. Ce projet est réalisé en parallèle de notre 
alternance.

L'objectif est de développer un jeu du Président en CLI (Interface graphique facultative), mais surtout de développer
des objets intéressants et fonctionnels.

Nous avons rencontré un certain nombre de difficultés dont voici une liste non exhaustive :

* Appréhender correctement les éléments nécessaires au fonctionnement du jeu, et les transformer en objets.

* La fonction `check_rules()` du fichier **PresidentGame.py**, qui a posé un défi algorithmique intéressant.

* La fonction `play()` du fichier **AIPlayer.py**, qui hérite de la classe **Player** et qui doit retourner une 
chaîne de caractères compréhensible par la classe **PresidentGame**.

Le développement de l'interface graphique est commencé et laissé ici pour montrer son avancée, mais le temps nous a 
manquer pour rendre le GUI fonctionnel.

## **Améliorations envisagées**

* Une interface graphique via la librairie **pygame**

* La possibilité de configurer la partie via l'interface (et non via le code source). Choisir le nombre de joueurs,
  d'IAs, changer les pseudos...

* Etendre le projet pour intégrer d'autres règles de jeu de cartes, et proposer une sélection du mode de jeu.