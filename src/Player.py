from src.Cards import Card


class Player:
    """
    Class representing the human player. Several methods for our player to interact with the game.
    """

    def __init__(self, name: str = None, hand: list = None):
        self.name = name
        self.hand = hand

    def add_to_hand(self, card: Card):
        """
        Add a card to player's hand.
        :param card: Card
        """
        self.hand.append(card)

    def remove_from_hand(self, card):
        """
        Remove a card from player's hand
        :param card: Card
        """
        for i in range(len(self.hand)):
            if card == self.hand[i]:
                self.hand.pop(i)
                break

    def sort_hand(self):
        """
        Sort the player's hand.
        :return: Sorted hand
        """
        hand_is_sorted = False
        while not hand_is_sorted:
            hand_is_sorted = True
            for i in range(1, len(self.hand)):
                if self.hand[i].value < self.hand[i - 1].value:
                    self.hand.insert(i - 1, self.hand[i])
                    self.hand.pop(i + 1)
                    hand_is_sorted = False

    def play(self, top_card: Card, nb_of_played_cards: int, action: str) -> str:
        """
        Implements logic to interact with PresidentGame.
        :param top_card: Last played card
        :param nb_of_played_cards: number of cards to play on this turn
        :param action: your turn to play a card, or you are presidente and you have to pick cards for trouduc
        """
        self.sort_hand()
        hand = self.hand
        if len(self.hand) > 5:
            hand = ""
            for card in range(len(self.hand)):
                hand += f"{self.hand[card]}, "
                if card % 5 == 0 and card != 0:
                    hand += "\n"
        print(f"Voici tes cartes {self.name} :\n{hand}\n")
        if action == "play a card":
            print(
                f"La carte précédement jouée est {'aucune' if top_card.value == 0 else top_card.name} et tu dois jouer {'le nombre que tu veux en' if nb_of_played_cards == 0 else nb_of_played_cards} carte(s)")
            print(
                "Laquelle veux-tu jouer? \n"
                "(Si c'est la première dans la liste de tes cartes écris 1, si c'est la deuxième écris 2, etc.. \n"
                "Si tu veux jouer une paire ou une triplette, fais comme ci-dessus avec un espace entre tes choix \n"
                "Si tu veux passer ton tour, envoies le mot passer) ")
            return input()
        if action == "give a card to trouduc":
            print("Quelle carte veux-tu donner au trouduc?")
            return input()

    def __repr__(self):
        return f"{self.name} et sa main {str(self.hand)}"
