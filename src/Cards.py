"""
card module
"""


class Card:
    """
    Generates instance of Card. Each instance have a name, a symbol and a value.
    """

    def __init__(self, name, symbol, value):
        self.name = name
        self.symbol = symbol
        self.value = value

    def __repr__(self):
        return f"{self.name} of {self.symbol} is worth {self.value} points"

    def __eq__(self, other):
        """
        Allow equal comparison with value.
        """
        return self.value == other.value

    def __gt__(self, other):
        """
        Allow greater than comparison with value.
        """
        return self.value > other.value

    def __lt__(self, other):
        """
        Allow lower than comparison with value.
        """
        return self.value < other.value
