"""
deck module
"""

import random

from src import Cards

NAMES_VALUES = {"Ace": 12, "2": 13, "3": 1, "4": 2, "5": 3, "6": 4, "7": 5, "8": 6, "9": 7, "10": 8,
                "Jack": 9, "Queen": 10, "King": 11}
SYMBOLS = ["♠", "♣", "♥", "♦"]


class Deck:
    """
    Generate a set of Cards. For each symbol we create all the cards and assign values to it.
    """

    def __init__(self):
        self.deck = [Cards.Card(name, symbol, value) for name, value in NAMES_VALUES.items() for symbol in SYMBOLS]

    def shuffle(self):
        """
        Shuffle the deck
        :return: Shuffled Deck.
        """
        iterations = 100
        while iterations > 0:
            index_to_switch = random.randint(0, len(self.deck) - 1)
            shuffled_card = self.deck[index_to_switch]
            self.deck[index_to_switch] = self.deck[0]
            self.deck[0] = shuffled_card

            iterations -= 1

    def __repr__(self):
        return f"len: {len(self.deck)} " + str(self.deck)
