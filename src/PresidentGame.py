from src.Cards import Card
from src.Deck import Deck
from src.AIPlayer import AIPlayer
from src.Player import Player
import pygame


class PresidentGame:
    """
    The whole gamemode and it's methods
    """

    def __init__(self):
        self.nb_of_played_cards = 0
        self.top_card = Card("first_card", "first_card", 0)
        self.game_deck = Deck()
        self.players = [Player("Human", []), AIPlayer("Vincenzo Bo Loré", []), AIPlayer("Geralt Dar Manin", [])]
        self.share_cards()
        self.chosen_cards = []
        self.remaining_players = 3
        self.presidente = None
        self.trouduc = None
        self.first_player = None
        self.player_turn = 0

    def __repr__(self):
        return f"Ceci est une partie avec {self.players} !"

    def start(self):
        """
        Starts the game
        """
        human_player = self.players[0]
        self.first_player = human_player

        # Partie de Président
        while True:
            self.apply_round_changes()
            self.remaining_players = 3

            # Manche de Président tant qu'il y a plus d'un joueur avec des cartes
            self.game_round()

            if input("Voulez-vous rejouer une manche?(yes/no)").lower() == "no":
                break
            else:
                self.top_card = Card("card", "card", 0)
                self.nb_of_played_cards = 0

    def check_rules(self, choice: str, player: Player) -> bool:
        """
        Check if the choice made by player is correctly written + if he has the right to play these cards.
        :param choice: choice from player
        :param player: the Player who plays
        :return: True if choice is correct, False either
        """
        if choice is not None and (choice.replace(" ", "").isnumeric()):  # le choix est-il non vide?
            choices = choice.split(" ")
            choices = [num for num in choices if num.isnumeric()]

            choices_set = set(choices)
            # le choix des cartes est-il correctement défini?
            if (all((len(player.hand) >= int(number) > 0) for number in choices)) and (
                    len(choices_set) == len(choices)):
                self.chosen_cards = [player.hand[int(number) - 1] for number in choices]
                chosen_cards_set = set(card.value for card in self.chosen_cards)
                # le set ne prenant pas de duplicats, si il n'y a qu'un element, le joueur n'a pas choisi deux cartes de valeurs différentes
                if len(chosen_cards_set) == 1:
                    # a-t-on le bon nombre de cartes jouées ou est-ce le premier tour?
                    if len(self.chosen_cards) == self.nb_of_played_cards or self.nb_of_played_cards == 0:
                        # les cartes choisies sont-elle assez fortes pour être jouées?
                        if self.chosen_cards[0].value >= self.top_card.value or self.top_card.value == 0:
                            return True
        return False  # si une des conditions n'est pas remplie

    def apply_played_card(self, chosen_cards, player: Player):
        """
        Put the card chosen by player as top card of the trash, define nb. of played cards on this turn, and remove
        cards from player's hand.
        :param: chosen_cards: cards the player chose
        :param: player: The player who played his card
        :return: The player without the card(s) he played
        """
        self.top_card = chosen_cards[0]
        self.nb_of_played_cards = len(chosen_cards)

        for card in self.chosen_cards:
            player.remove_from_hand(card)

    def share_cards(self):
        """
        Display cards to players depending on the amount of players.
        """
        to_discard = 52 % len(self.players)
        self.game_deck.shuffle()
        increment = 0
        # distribution des cartes
        while increment < 52 - to_discard:
            for player in range(len(self.players)):
                self.players[player].add_to_hand(self.game_deck.deck[increment])
                increment += 1

    def do_the_cards_exchange(self, trouduc: Player, presidente: Player):
        """
        The trouduc gives his 2 best cards to the President, the President gives two cards to the trouduc.
        :param trouduc: the bad player
        :param presidente: the GOAT
        """
        for i in range(2):
            best_card = Card("card", "card", 0)
            for card in trouduc.hand:
                if card.value > best_card.value:
                    best_card = card
            presidente.add_to_hand(best_card)
            trouduc.remove_from_hand(best_card)
        for i in range(2):
            while True:
                choice = presidente.play(self.top_card, self.nb_of_played_cards, "give a card to trouduc")
                if self.check_rules(choice, presidente) and choice != "passer":
                    trouduc.add_to_hand(self.chosen_cards[0])
                    presidente.remove_from_hand(choice)
                else:
                    print("Tu ne peux pas faire ça")

    def apply_round_changes(self):
        """
        Check players number and player's hands before designating President and Trouduc, then apply all the changes.
        """
        if self.presidente is not None and self.trouduc is not None:
            for player in range(len(self.players)):
                self.players[player].hand = []
                if self.players[player].hand:
                    self.trouduc = self.players[player]
                    self.first_player = self.trouduc
            self.share_cards()
            self.do_the_cards_exchange(self.trouduc, self.presidente)
        else:
            self.top_card = Card("", "", 0)
            self.nb_of_played_cards = 0
        self.trouduc = None
        self.presidente = None

    def game_round(self):
        """
        This method implements full course of the round. Use al the other methods to drive the game round.
        """
        while self.remaining_players > 1:
            print("--------------------- Nouveau tour de table ! -----------------------")
            pass_the_turn: int = 0  # permet de savoir si les 3 joueurs ont passé ou ne peuvent pas jouer
            # change-t-on de premier joueur?
            if not self.first_player == self.players[0]:
                self.players.pop(self.players.index(self.first_player))
                self.players.insert(0, self.first_player)
            self.chosen_cards = []
            for player in self.players:
                choice_is_correct = False
                if len(player.hand) > 0 and any(card.value >= self.top_card.value for card in player.hand):
                    while not choice_is_correct:
                        choice = player.play(self.top_card, self.nb_of_played_cards, "play a card")
                        if choice == "passer":
                            print(f"{player.name} a décidé de passer son tour!")
                            pass_the_turn += 1
                            break
                        if self.check_rules(choice, player):
                            choice_is_correct = True
                            self.apply_played_card(self.chosen_cards, player)
                            print(
                                f"{player.name} joue {self.nb_of_played_cards} cartes {self.chosen_cards[0].name}")
                            if not player.hand:
                                if self.remaining_players == 3:
                                    self.presidente = player
                                self.remaining_players -= 1
                        else:
                            print("Tu n'as pas le droit de faire ça!! ")
                    # si un 2 a été posé
                    if self.top_card.value == 13:
                        print("un 2 a été joué, tour terminé!!!")
                        self.apply_round_changes()
                        self.first_player = player
                        break
                else:
                    print(f"{player.name} ne peux pas jouer")
                    pass_the_turn += 1
            if pass_the_turn == 3:
                self.apply_round_changes()
