from src.Player import Player
from src.Cards import Card
from operator import attrgetter


class AIPlayer(Player):
    """
    Inherits from class Player. AIPlayer can play their best card and follow double, triple and square.
    """

    def __init__(self, name: str = None, hand: list = None):
        super().__init__(name, hand)
        self.name = name
        self.hand = hand

    def play(self, top_card: Card, nb_of_played_cards: int, action: str) -> str:
        """
        This allow AIPlayer to use some logic and don't play randomly.
        :param top_card: Last played card.
        :param nb_of_played_cards: int.
        :param action: round prompt.
        :return: another action.
        """
        if action == "play a card":
            if top_card.value == 0 and nb_of_played_cards == 0:
                worst_card = min(self.hand, key=attrgetter('value'))
                i = self.hand.index(worst_card)
                return str(i + 1)
            elif top_card and nb_of_played_cards >= 1:
                for i in range(top_card.value, 14):
                    cards_list = []
                    for j in range(len(self.hand)):
                        if self.hand[j].value == i:
                            cards_list.append(j)
                    if len(cards_list) >= nb_of_played_cards:
                        response = ""
                        for x in range(0, nb_of_played_cards):
                            response += str(cards_list[x] + 1) + " "
                        return response
                return "passer"
            else:
                return "passer"
        if action == "give a card to trouduc":
            pass
