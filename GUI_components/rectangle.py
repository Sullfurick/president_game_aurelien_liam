from src import Cards


class CardRectangle:
    def __init__(self, x: int, y: int, color, z_order: int, card: Cards.Card):
        self.x = x
        self.y = y
        self.color = color
        self.z_order = z_order
        self.card = card
