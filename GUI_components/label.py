import pygame
from pygame.font import Font


class Label:
    def __init__(self, x: int,
                 y: int,
                 font: pygame.font.Font,
                 text_color,
                 background,
                 text: str = None,
                 underlined: bool = False,
                 bold: bool = False,
                 italic: bool = False):
        font.set_underline(underlined)
        font.set_italic(italic)
        font.set_bold(bold)
        self.x = x
        self.y = y
        self.text = font.render(text if text else "", True, text_color, background)
