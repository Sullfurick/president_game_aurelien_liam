from GUI_components.label import Label
from src.Player import Player
from src.PresidentGame import PresidentGame
import pygame

president_game = PresidentGame()
play_with_terminal = True
while True:
    choice = input("Voulez-vous jouer en terminal ou interface graphique? (y pour terminal, n pour graphique(WIP))")
    if choice == "n" or choice == "y":
        if choice == "n":
            play_with_terminal = False
        break
if play_with_terminal:
    president_game.start()

if not play_with_terminal:
    BACKGROUND_COLOR = 234, 239, 255
    pygame.init()
    screen = pygame.display.set_mode((800, 500))
    pygame.display.set_caption("Président")
    running = True

    font = pygame.font.Font('freesansbold.ttf', 20)
    # Labels
    president_label = Label(x=10, y=20, text="Président:", font=font, text_color=(0, 0, 0), background=BACKGROUND_COLOR,
                            underlined=True)
    president_name = Label(x=10, y=50, text=president_game.presidente, font=font, text_color=(0, 0, 0),
                           background=BACKGROUND_COLOR)
    trouduc_label = Label(x=10, y=90, text="Trouduc:", font=font, text_color=(0, 0, 0), background=BACKGROUND_COLOR,
                          underlined=True)
    trouduc_name = Label(x=10, y=120, text=president_game.trouduc, font=font, text_color=(0, 0, 0),
                         background=BACKGROUND_COLOR)

    label_list = [president_label, president_name, trouduc_name, trouduc_label]
    cards_list = []
    chosen_cards = []
    can_play = False

    while running:
        current_player: Player = president_game.players[president_game.player_turn]
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                pos = pygame.mouse.get_pos()
                for card in cards_list:
                    if card.collidepoint(pos):
                        chosen_cards.remove(card) if card in chosen_cards else chosen_cards.append(card)
                choice = ""
                for chosen_card in chosen_cards:
                    choice += f"{current_player.hand.index(chosen_card) + 1}"
                can_play = True is president_game.check_rules(choice, current_player)

            pygame.display.update()
        screen.fill(BACKGROUND_COLOR)

        for label in label_list:
            screen.blit(label.text, (label.x, label.y))

        pygame.draw.rect(screen, (0, 0, 0),
                         pygame.Rect(0, 200, 800, 300), 2)

        pygame.display.flip()

    pygame.quit()
